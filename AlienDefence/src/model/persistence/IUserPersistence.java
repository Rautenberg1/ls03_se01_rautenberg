package model.persistence;

import view.menue.User;

public interface IUserPersistence {

	User readUser(String username);

}